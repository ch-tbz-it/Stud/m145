# Netzwerkdokumentation

Warum brauchen wir Netzwerkdokumentationen? Das ist eine gute Frage, besonders wenn man den Aufwand betrachtet, der für ihre Erstellung und Aktualisierung nötig ist. Eine Netzwerkdokumentation sammelt systematisch alle Beschreibungen und Merkmale eines Netzwerks. Für Netzwerkadministratoren ist sie sehr wichtig. Ohne sie müsste man vor Ort herausfinden, welche Geräte verwendet werden, ihre Einstellungen und so weiter.

Was sind die Vorteile?
 - Einfachere Verwaltung und Fehlerbehebung einfacher.
 - Unterstützt bei der Planung von Netzwerkerweiterungen.
 - Überblick über die verwendeten Komponenten.
 - Hochwertige Arbeit der IT-Verantwortlichen.

## Theorie
 - [Theorie](./Theorie/README.md)

## Übungen
 - [Netzwerkdokumentation Interpretieren](./02_Interpretieren/README.md)

 # Handlungskompetenzen
 - 1 Dokumentation eines Netzwerkes interpretieren und nachführen.
 - 1.1 Kennt Aufbau und Inhalt einer Netzwerkdokumentation (Netzwerkplan und Netzwerkschema, Konfigurationen und Inventarlisten).
 - 1.2 Kennt gängige Darstellungsarten und Symbole für Netzwerkplan und Netzwerkschema