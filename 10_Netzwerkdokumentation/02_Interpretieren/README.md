# Übung: Netzwerkdokumentation richtig interpretieren

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie können eine Netzwerkdokumentation richtig interpretieren |

[Netzwerkdokumentation](./media/Netzwerkdoku1.pdf) interpretieren:

* Wie lautet die Netzwerkadresse vom Standort Samedan?
* Auf welcher IP-Adresse befindet sich der AccessPoint in Bellinzona?
* In welchem VLAN befinden sich die Arbeitsplatz-PCs?
* Über welche IP-Adresse erreicht man den Manageable-Switch am Standort Chur?
* Welche IP-Adressen LAN-seitig und tunnelseitig ist der VPN-Gateway am Standort Bellinzona konfiguriert?
* Wie lautet der am Standort Samedan an den Arbeitsplatz-PCs eingetragene Standardgateway?
* Ein Aussendienstmitarbeiter benötigt Zugriff auf das Firmennetzwerk. Wie muss er seine VPN-SW IP-mässig konfigurieren?
* Wer hat im Büro CAD Wasserbau die VoIP-Telefone installiert?
* Wann wurde im Bistro der AccesPoint installiert?
* Der IT-Mitarbeiter Rene Sauter (rsauter) verlässt die Firma. Welche Arbeiten bzw. Verantwortlichkeiten sind davon betroffen? Wenn würden sie als zukünftigen Ansprechpartner bei Problemen vorschlagen?
* Wieviele RJ45-Steckdosen stehen ihnen im Bauleiterbüro zur Verfügung und wieviele davon sind zurzeit noch nicht belegt?
* Im Büro CAD Tiefbau muss ein weiteres VoIP-Telefon zur Verfügung stehen. Wie soll diese Verbindung gepatched werden? Verlangt wird die Patchpanelbelegung und der Switch-Port.
* Im Bistro soll eine Projektpräsentation stattfinden. Dafür muss ein Ethernetkabelverbindung bereitgestellt werden.
* Im Büro CAD Wasserbau soll temporär ein weiterer Arbeitsplatz eingereichtet werden. Wie werden sie diese Aufgabe lösen?
* Wieviele Switchs stehen ihnen am Standort Chur zur Verfügung?
* Frau Sommer arbeitet an der CAD-Workstation und meldet ihnen, dass sie keine Verbindung ins Internet mehr hat. Was werden sie überprüfen?
* Wie lautet die SSID des Churer-AccessPoint?