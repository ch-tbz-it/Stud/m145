# Netzwerkdokumentation


Eine Netzwerkdokumentation ist ein umfassendes Dokument, das alle wichtigen Informationen über ein Netzwerk enthält. Sie dient als Referenz für IT-Administratoren, Netzwerktechniker und andere Fachleute, die mit dem Netzwerk arbeiten. Eine gute Netzwerkdokumentation ist entscheidend für die effiziente Verwaltung, Wartung und Fehlerbehebung eines Netzwerks.

## Aktualität und Lesbarkeit

Wie oft muss die Dokumentation nachgeführt werden? <br> Hier gibt es nur eine 
klare Antwort: Bei jeder Änderung oder Anpassung. Die verschiedenen Dokumente 
sind in einer Liste ersichtlich. Die Dokumentation soll verständlich und logisch 
aufgebaut sein (Bezeichnungen, Kürzel, Nummerierung)

## Zugänglichkeit, Verantwortlichkeit

Es muss klar definiert sein, wer für welche Dokumente die Verantwortung hat. Die 
aktuelle Dokumentation muss für die betreffenden Personen immer und sofort 
zugänglich sein. Der Standort der Dokumentation muss allen bekannt sein.


## Betriebsinformationen

Folgende Informationen sollten in der Dokumentation enhalten sein:

* Logisches Netzlayout, Netzwerkdiagramm, Übersicht des Systems
* Physische Netzstruktur, Verkabelungsplan, Raumnummerierung, Anschlussbelegung, Geräteposition,
* Inventar HW (alle Geräte) und SW (alle Programme)
* Konfiguration der Netzwerkdienste (inkl. Accountmanagement)
* Konzept der Adressierung und Namensgebnng
* Verträge mit externen Firmen, Dienstleister (Auftragsnehmer), Lieferanten
* Identifikation aller Beteiligten: Adressen, Erreichbarkeit, Kommunikationskanäle, Anfahrtswege, Funktion in der Netzwerkverwaltung
* Handlungsanweisungen für wiederkehrende Aufgaben


## Begriffsdefinitionen

**Netzwerkplan**: Ein Netzwerkplan ist ein umfassendes Dokument, das die Architektur und Struktur eines Netzwerks beschreibt, einschließlich der physischen und logischen Komponenten sowie deren Beziehungen zueinander. Ein Netzwerkplan kann verschiedene Elemente enthalten, wie z.B. Netzwerktopologien, IP-Adressierungsschemata, Sicherheitsrichtlinien und Dienstbeschreibungen.

**Netzwerkschema**: Ein Netzwerkschema ist eine grafische Darstellung der physischen Topologie eines Netzwerks, die die Anordnung und Verbindung der Netzwerkgeräte zeigt. Es bietet eine visuelle Übersicht über die physische Struktur des Netzwerks, einschließlich der Platzierung von Switches, Routern, Servern und anderen Geräten.

**Logisches Netzlayout**: Das logische Netzlayout beschreibt die Struktur eines Netzwerks auf einer abstrakten Ebene, die unabhängig von der physischen Implementierung ist. Es umfasst die logische Anordnung von Netzwerkkomponenten wie Subnetzen, VLANs und Routing-Protokollen, um die Effizienz und Sicherheit des Netzwerks zu optimieren.

**Netzwerkdiagramm**: Ein Netzwerkdiagramm ist eine grafische Darstellung eines Netzwerks, das die Verbindungen zwischen verschiedenen Netzwerkgeräten und -komponenten visualisiert. Es kann verschiedene Arten von Informationen enthalten, wie z.B. physische Topologie, IP-Adressierungsschemata, Dienstbereitstellung und Sicherheitsrichtlinien.

**Physische Netzstruktur**: Die physische Netzstruktur bezieht sich auf die tatsächliche Hardwarekomponenten und Verbindungen, aus denen das Netzwerk besteht. Dies umfasst Switches, Router, Server, Endgeräte und die physische Verkabelung, die verwendet wird, um diese Geräte miteinander zu verbinden.

**Verkabelungsplan**: Ein Verkabelungsplan ist ein Dokument, das die spezifischen Details der physischen Verkabelung eines Netzwerks beschreibt. Dies umfasst die Art der verwendeten Kabel (z.B. Ethernet, Glasfaser), die Länge der Kabel, die Verbindungspunkte und die Anordnung der Verkabelung in Gebäuden oder Rechenzentren.

## Gängige Darstellungsarten und Symbole für Netzwerkplan