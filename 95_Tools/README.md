# Tools

## Cisco Packet Tracer
Packet Tracer ist ein plattformübergreifendes visuelles Simulationswerkzeug, das von Cisco Systems entwickelt wurde und es Benutzern ermöglicht, Netzwerk-Topologien zu erstellen und moderne Computernetzwerke zu imitieren. Die Software ermöglicht Benutzern, die Konfiguration von Cisco-Routern und -Switches mit einer simulierten Befehlszeilenschnittstelle zu simulieren. Packet Tracer verwendet eine Drag-and-Drop-Benutzeroberfläche, die es den Benutzern ermöglicht, simulierte Netzwerkgeräte nach Belieben hinzuzufügen und zu entfernen.

Quelle: https://en.wikipedia.org/wiki/Packet_Tracer

### Installation auf dem eigenen Notebook

Damit der Cisco Packet Tracer heruntergeladen und verwendet werden kann, wird ein *Skills For All*-Account benötigt. 

1. Webseite https://www.netacad.com/courses/packet-tracer besuchen
2. Unter "Cisco Packet Tracer" => "View Courses" wählen
3. Unter "All Content" => "Getting started with Cisco Packet Tracer" wählen. 
4. Sprache auswählen (Englisch empfohlen) => Einloggen oder Account erstellen
5. https://skillsforall.com/resources/lab-downloads besuchen und die entsprechende Version herunterladen.

![Übersicht Herunterladen Cisco Packet Tracer](media/overview_download.PNG)

### Einführungskurs
Eine gute Einführung in den Cisco Packet Tracer:
[https://skillsforall.com/course/getting-started-cisco-packet-tracer?courseLang=en-US](skillsforall.com Getting Started with Cisco Packet Tracer)

