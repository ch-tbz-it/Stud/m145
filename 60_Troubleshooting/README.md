# Troubleshooting

# Handlungskompetenzen
 - 3 Bei Störungen Fehlersymptome und -meldungen systematisch erfassen. Ursachen von Störungen mit Tools ermitteln und beheben.
 - 3.1 Kennt die wichtigsten Indizien bzw. Symptome, welche auf Störungen hinsichtlich Verfügbarkeit und Performance hinweisen können.
 - 3.2 Kennt Methoden um Störungen systematisch zu ermitteln und beheben.