# WLAN


# Theorie


# Übungen
 - Eigene Wohnung ausmessen
 - Cisco Packet Tracer Übung zu WLAN

# Handlungskompetenzen
 - 5 Netzwerke mit Wireless LAN erweitern und mit gesichertem Zugang konfigurieren.
 - 5.1 Kennt WLAN Standards, Antennenarten und typische Einsatzgebiete. sowie deren Vorteile und Nachteile
 - 5.2 Kennt WLAN-Sicherheitsmassnahmen