# WLAN - Die eigene Wohnung ausmessen

![Heatmapper](media/heatmapper-1_1-1-19.jpg)

| Lernauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Zeitbudget  |  0.5h Vorbereitung, 0.5h Durchführung *Survey*, 0.5h Nachbearbeitung |
| Lernziel | Wireless Site Survey Tools kennenlernen und Erfahrungen mit der WLAN Signalverteilung sammeln |


Die Abdeckung eines Wireless-LAN unterliegt einer Vielzahl von Einflüssen, welche selber für Erfahrene oft nicht vorhersehbar sind. 
Um die Planung insbesondere von grösseren WLAN-Installationen zu vereinfachen, gibt es nützliche Tools:
 - Tools in dem die Grundrisse mit allen Baumaterialien hinterlegt werden können, um die WLAN Signalverteilung zu abzuschätzen. 
 - [*Wireless Site Survey Tools*](https://en.wikipedia.org/wiki/Wireless_site_survey) mit denen die Signalverteilung gemessen wird

In dieser Übung arbeiten wir mit einem *Wireless Site Survey Tool* um das WLAN unseres zu Hause auszumessen. 

## Ziel
In dieser Übung soll die Signalqualität in der eigenen Wohnung / Haus ausgemessen und dokumentiert werden. 

## Lernprodukt
A4 Dokument (PDF) oder Seite in Markdown mit folgenden Inhalt:
 - Grundriss der Wohnung / Haus mit Heatmap
 - Eigene(r) WLAN Accesspoint(s) markiert
 - Marke + Typ des WLAN Accesspoint(s)

## Tools
 - *Ekahau Heatmapper* (Windows)- Verfügbar über Sharepoint
 - *Net Spot FREE Edition* (MacOs) - [www.netspotapp.com](https://www.netspotapp.com/de/)

## Vorgehen
 1. *Grundriss vorbereiten*: Bestehende Grundrisszeichnung als Bilddatei auf den eigenen Computer laden *oder* <br/> Grundriss mithilfe von [draw.io](https://draw.io) erstellen
 2. *Wireless Site Survey Tools* auswählen (siehe Tools) und auf eigenen Computer installieren.
 3. Grundriss als Bilddatei in die gewünschte Software einlesen.
 4. Durch die Wohnung laufen und an diverse Punkten in der Wohnung auf dem Grundriss an den entsprechenden Punkt klicken.
 5. Heatmap abspeichern (Screenshot machen)
 6. Dokument mit Heatmap und den geforderten Informationen (siehe Lernprodukt) erstellen. 

