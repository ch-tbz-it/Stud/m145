# Übung: BeASwitch

Switch (*früher Switching Hubs*) bezeichnet ein Kopplungselement in Rechnernetzen, das Netzwerksegmente miteinander verbindet. Ein Switch arbeitet in der Regel auf OSI-Layer 2. Es sorgt innerhalb eines Segments (Broadcast-Domain) dafür, dass die Datenpakete, sogenannte „Frames“, an ihr Ziel kommen. [*Abgeändert* Wikipedia][1]

Mit dem Lernprogramm BeASwitch übernimmt die lernende Person die Rolle des Mikrochips innerhalb eines Switches. Die Prämisse der Übung: Ist die Person in der Lage die Switching-Entscheidungen zu treffen, so versteht sie auch die zugrundeliegende Logik eines Ethernet-Switches. 

**Tipp für schnellen Lernerfolg:** Gleich mit dem Lernprogramm starten und auf spielerische Weise Switching verstehen.

# BeASwitch
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie verstehen die grundlegende Funktion eines Switches. |

## Vorgehen
 - Laden Sie sich auf https://github.com/muqiuq/BeASwitch/releases die neuste Version  der Applikation *BeASwitch* herunter.
 - Starten Sie das EXE (Anwendung ist nicht signiert und Windows gibt eine entsprechende Warnmeldung aus)<br>**Eventuell müssen Sie noch .NET Core 3.1 installieren.**
 - Spielen Sie die Anwendung so lange durch bis Sie keine Fehler mehr machen. 
 - Ziel ist es 30 von 30 Switching-Entscheidungen richtig zu machen.

**Hinweis: Spielen Sie mit BeASwitch als ob es ein Rätsel wäre. Sie brauchen keine speziellen IT-Kenntnisse, um herauszufinden wie die Anwendung funktioniert.**

![Screenshot BeASwitch](media/BeASwitch_Explanation.PNG)

## Didaktische Überlegung
 - Durch Beobachten erkennt der/die Lernende die Logik der Grundfunktionalität eines Switches: Das Weiterleiten von Frames mithilfe der *Destination MAC*, *Source MAC* und *MAC*-Tabelle. 
 - Der/die Lerende prüft sein Verständnis über die Switching Logik indem er/sie Entscheidungen für im Switch ankommende Frame fällt und anschliessend mithilfe des Programmes überprüfen kann, ob das Resultat seiner Überlegung korrekt ist. 

# Quellen
 - [1]: https://de.wikipedia.org/wiki/Switch_(Netzwerktechnik) "Wikipedia DE"