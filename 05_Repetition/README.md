# Repetition

Als Grundlage für das Modul *145 Netzwerk betreiben und erweitern* dienen die Module
 - 117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren
 - 123 Serverdienste in Betrieb nehmen
 - 129 LAN-Komponenten in Betrieb nehmen

Diverse in diesen Modulen erlernte Handlungskompetenzen werden im Modul 145 angewendet. Insbesondere:
 - Kennt die Funktionsweise von Switch und Router und deren Einsatzgebiete.
 - Kennt Symbole zur schematischen Darstellung von Netzwerken.
 - Kennt die Elemente und Funktionen des IP-Protokolls (MAC- und IP-Adressen, IP-Adressklassen, private Adressen, Netzmasken, Routing, Adress Resolution Protocol (ARP)).
 - Kennt Gründe für die Aufteilung eines Netzwerks in IP-Subnetze.
 - Kennt die Algorithmen zur (binären) Berechnung von IP-Subnetzen.
 - Kennt den Aufbau und den Inhalt von Routingtabellen und den Zusammenhang zum Netzwerkschema.
 - Kennt Verfahren zur systematischen Eingrenzung von Fehlern im Netzwerk (z.B. Ausschlussverfahren, Einordnung im OSI-Schichtenmodell).
 - Kennt Werkzeuge zur Fehleranalyse und -behebung und weiss, bei welchen Symptomen welche Werkzeuge eingesetzt werden.$
 - Kennt Aufbau und Inhalt einer Netzwerkdokumentation.

Diese Repetition deckt folgende Handlungskompetenzen ab.

## Repetition Theorie
 - [01_Repetitions Quiz](https://forms.office.com/e/BeMZnK0ppw)
 - [02_Subnetting Quiz 1](https://forms.office.com/e/9c1fqtJ4yP)
 - [03_Subnetting Quiz 2](https://forms.office.com/e/fG7geBiawG)

## Übungen
 - [02_BeASwitch](02_BeASwitch/README.md) - Wer Switching versteht kann auch ein Switch sein. 
 - [03_BeARouter](03_BeARouter/README.md) - Wer ~~Switching~~Routing versteht kann auch ein ~~Switch~~Router sein.  
 - [04_IPv4 Quiz](04_IPv4_Quiz/README.md) - Mit Repetition IPv4 Berechnungen beherrschen.

## Laborübungen
 - [05_Cisco Packet Tracer Übungen](05_PacketTracer/README.md)

