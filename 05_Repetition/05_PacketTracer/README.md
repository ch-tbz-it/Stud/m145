# Cisco Packet Tracer Repetitions-Übungen

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung mit Cisco Packet Tracer |
| Zeitbudget  |  6 Lektion |
| Ziele | Grundlagen von vorangehenden Netzwerkmodulen repetiert und praktische Erfahrungen mit dem Cisco Packet Tracer gesammelt |

---

Alle aufgeführten Cisco Packet Tracer Labore findest du auf dem Sharepoint zum Modul. 

Link zum entsprechenden Ordner: [students/it/_read-only/M145/04 - Cisco Packet Tracer/05_Repetition](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/it/_read-only/M145/04%20-%20Cisco%20Packet%20Tracer/05_Repetition?csf=1&web=1&e=5nW8qO).

**Hinweis:** Falls der Link zum Sharepoint nicht funktioniert, stelle sicher, dass du mit dem TBZ Microsoft Login eingeloggt bist. Falls der Link immer noch nicht funktioniert, frage bei der Lehrperson nach den entsprechenden Dateien. 

## 01_Packet Tracer - Navigate the IOS

Falls du wenig Erfahrung mit dem Cisco Packet Tracer und Cisco IOS CLI (Command Line Konfiguration von professionellen Cisco Geräten) hast, gibt dir diese Laborübung eine kurze Einführung in die wichtigsten Befehle. 

## 02_Packet Tracer - Configure Initial Switch Settings

In dieser Übung lernst du, wie du einen Switch mit Authentifizierung absicherst. 

Diese Übung deckt die Handlungskompetenz Vom Modul 129 *1.5 Kennt die Sicherheitsschwachstellen von Switch und Routern (z.B. Defaultpasswort, telnet).* ab. 

## 03_Packet Tracer - Implement Basic Connectivity

Diese Übung dient zur Repetition vom Handlungsziel des Modul 117 *Netzwerk aufbauen und die Netzwerkkomponenten gemäss Herstellerdokumentationen installieren und konfigurieren.*

In dieser Übung konfigurierst du zwei Netzwerkgeräte und lässt sie miteinander Kommunizieren (ping).

## 04_Packet Tracer - Identify MAC and IP Addresses

Repetition zum Thema MAC und IP-Adressen. 

Diese Übung deckt die Handlungskompetenz Vom Modul 129 *2.1 Kennt die Elemente und Funktionen des IP-Protokolls (MAC- und IP-Adressen, IP-Adressklassen, private Adressen, Netzmasken, Routing, Adress Resolution Protocol (ARP)).* ab. 

## 05_Packet Tracer - Examine the ARP Table

Repetition zum wichtigsten "Verbindungsstück" zwischen MAC und IP: Address Resolution Protocol (ARP)

Diese Übung deckt die Handlungskompetenz Vom Modul 129 *2.1 Kennt die Elemente und Funktionen des IP-Protokolls (MAC- und IP-Adressen, IP-Adressklassen, private Adressen, Netzmasken, Routing, Adress Resolution Protocol (ARP)).* ab. 

## 06_Packet Tracer - Subnet an IPv4 Network

Willst du wissen, wie gut du IPv4-Subnetting verstanden hast? Dann ist diese Übung das richtige für dich! 

Sie deckt aus dem Handlungsziel vom Modul 129 *2. Adressschema für IP Netz mit Subnetzen anpassen und geeignetes Subnetting mit zugehöriger Netzmaske aus Vorgaben ableiten (z.B. Aufteilung in IP Netze, Anzahl Clients).* die Handlungskompetenzen
 - 2.2. Kennt Gründe für die Aufteilung eines Netzwerks in IP-Subnetze.
 - 2.3. Kennt die Algorithmen zur (binären) Berechnung von IP-Subnetzen.

ab. 

## Abgabe der Übungen
Die Abgabemodalitäten sind abhängig von der Lehrperson. Informiere dich bei der Lehrperson, welche Aufgaben in welcher Form abzugeben sind. 