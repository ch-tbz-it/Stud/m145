# Übung: BeARouter

Die Übung **BeARouter* entspricht vom Prinzip der vorhergehenden Übung BeARouter. Anstatt Switching-Entscheidungen zu treffen, sind nun IPv4-Routing-Entscheidungen zu treffen. 

**Tipp für schnellen Lernerfolg:** Gleich mit dem Lernprogramm starten und auf spielerische Weise Routing verstehen.

![Screenshot BeARouter](media/BeARouter.PNG)

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Sie verstehen das grundlegende Funktionsprinzip von Routing. |

## Vorgehen
 - Laden Sie sich auf https://github.com/muqiuq/BeASwitch/releases die Version neuste Version der Applikation *BeARouter* herunter.
 - Starten Sie das EXE (Anwendung ist nicht signiert und muss manuell vertraut werden!)<br>**Eventuell müssen Sie noch .NET Core 3.1 installieren.**
 - Spielen Sie die Anwendung so lange durch bis Sie keine Fehler mehr machen. 
 - Ziel ist es 50 von 50 Routing-Entscheidungen richtig zu machen.


![Screenshot BeARouter mit Erklärungen](media/BeARouter_Explanation.PNG)