# Übung: IPv4 Quiz

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Sie verstehen das grundlegende Funktionsprinzip von Routing. |

Um IPv4 korrekt auf Routern zu konfigurieren, sind die wichtigsten IPv4 Berechnungen zu beherrschen:
 - Broadcast Adresse eines Subnetzes berechnen
 - Netzwerk Adresse eines Subnetzes berechnen
 - Anzahl möglicher Host Adresse eines Subnetzes berechnen
 - Subnetzmasken-Umrechnung von Prefix (Bsp: "/24") zur Dezimal Darstellung (Bsp: 255.255.255.0) (und umgekehrt)
 - Ein vorgegebenes Subnetz in weitere Subnetze aufteilen. 

Alle diese Aufgaben können mit dem [*Visual Subnet Calculator*](https://www.davidc.net/sites/default/subnets/subnets.html) gelöst werden. 

* Ziel ist es 30 von 30 Routing-Entscheidungen richtig zu machen.

Weiterführende Inhalte: https://gitlab.com/ch-tbz-it/Stud/m129/-/blob/main/50_IPv4/README.md

# Vorgehen
 - **Voraussetzung:** Die Anwendung BeARouter aus Übung 2 ist auf Ihrem PC installiert und funktionstüchtig. 
 - Nach dem Starten der Anwendung ist die Schaltfläche *Quiz* verfügbar. Mit einem Klick auf diese Schaltfläche wird die Anwendung *DoAIPv4Quiz* gestartet. 
 - *Hinweis:* Die Schaltfläche ist nur direkt nach dem Start der Anwendung sichtbar!

![BeARouter IPv4 Quiz](media/IPv4Quiz.PNG)