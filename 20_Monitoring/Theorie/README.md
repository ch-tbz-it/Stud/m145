# Monitoring - Überwachung & Auswertung

Ein Netzwerk ist eine Sammlung von Geräten, die miteinander verbunden sind und über ein gemeinsames Transport- oder Kommunikationsprotokoll miteinander kommunizieren können. Hier kann Kommunikation sich auf den Datenaustausch zwischen Benutzern oder Anweisungen zwischen Knoten im Netzwerk beziehen, wie z. B. Computern, Mobilgeräten, Ausgabegeräten, Managementelementen, Servern, Routing- und Switching-Geräten usw.

Hier sind einige der grundlegenden Komponenten, die Teil jedes Computernetzwerks sind und gleichzeitig die Grundlagen für die Netzwerküberwachung bilden.
* IP Addresses and Subnetting
* Switching und Routing
* DNS
* DHCP

Netzwerküberwachung ist eine wichtige Funktion im Netzwerkmanagement, weil sie drei der fünf Ziele im Netzwerkmanagement erreicht: Leistungsüberwachung, Fehlerüberwachung und Kontoüberwachung.

## Allgemeine Überwachungstechniken und Protokolle

Um Ihr Netzwerk oder sogar Server und Systeme erfolgreich zu überwachen, ist die Verfügbarkeit der folgenden Optionen erforderlich:

* Daten oder Informationen von verschiedenen Elementen im Netzwerk. Die Daten umfassen Informationen über die Funktionsweise, den aktuellen Status und die Leistung sowie die Gesundheit des zu überwachenden Elements.
* Eine Anwendung oder Überwachungssoftware muss in der Lage sein, Daten in einem benutzerfreundlichen Format zu sammeln, zu verarbeiten und darzustellen. Die Software sollte Benutzer sogar über bevorstehende Probleme auf der Grundlage von Schwellenwerten informieren.
* Ein Protokoll oder eine Methode zum Übertragen von Informationen zwischen dem überwachten Element und der Überwachungssoftware.

Die aus dem Netzwerk gesammelten Informationen helfen bei einer besseren Verwaltung und Kontrolle des Netzwerks, bei der Identifizierung möglicher Netzwerkprobleme, bevor sie zu Ausfallzeiten führen, und bei der schnellen Lösung von Problemen, wenn etwas schief geht. Kurz gesagt, eine kontinuierliche Überwachung trägt dazu bei, ein leistungsfähiges Netzwerk zu schaffen.

Nachfolgend sind einige der allgemeinen Techniken aufgeführt, die für die Überwachung zur Verfügung stehen. Diese Techniken werden zur Erfassung von Überwachungsdaten aus dem Netzwerk verwendet.

### Ping
Dies ist ein Netzwerk-Admin-Tool, das verwendet wird, um die Erreichbarkeit und Verfügbarkeit eines Hosts in einem IP-Netzwerk zu testen. Die Daten aus den Ping-Ergebnissen können bestimmen, ob ein Host im Netzwerk aktiv ist oder nicht. Darüber hinaus kann es die Übertragungszeit und Paketverluste beim Kommunizieren mit einem Host messen.


### Simple Network Management Protocol (SNMP)
SNMP ist ein Netzwerkverwaltungsprotokoll, das zum Austausch von Informationen zwischen Hosts in einem Netzwerk verwendet wird, das Netzwerküberwachungssoftware enthält. Dies ist das am weitesten verbreitete Protokoll für die Verwaltung und Überwachung des Netzwerks und umfasst die folgenden Komponenten:

1. Verwaltetes Gerät: Der Knoten im Netzwerk, der SNMP unterstützt und Zugriff auf spezifische Informationen ermöglicht.
2. Agent: Eine Software, die Teil des überwachten Geräts ist. Ein Agent hat Zugriff auf die  MIB (Management Information Database) des Geräts und ermöglicht NMS-Systemen das Lesen und Schreiben in die MIB.
3. Netzwerkverwaltungssystem (NMS- Network Management System): Eine Anwendung auf einem System, die die verwalteten Geräte über den Agenten mithilfe von SNMP-Befehlen überwacht und steuert.

SNMP-Daten werden entweder durch Abfragen (Polling) oder durch Verwendung von Traps an ein verwaltetes Gerät gesammelt oder gesendet. Traps ermöglichen es einem Agenten, Informationen über Ereignisse auf dem Gerät an ein NMS zu senden.

Die MIB (Management Information Database) enthält Informationen über die Struktur der Daten auf einem Gerät für das Management. Die MIBs enthalten OID (Objektidentifikatoren), die die eigentliche Kennung für die Variable darstellen, die vom Gerät gelesen oder auf dem Gerät festgelegt werden soll.

### Syslog
Syslog (nicht zu verwechseln mit Windows Eventlog) ist ein Nachrichtenprotokollsystem, das einem Gerät ermöglicht, Ereignisbenachrichtigungen in IP-Netzwerken zu senden. Die Informationen aus diesen Nachrichten können für das Systemmanagement sowie für Sicherheitsprüfungen verwendet werden. Syslogs werden von einer Vielzahl von Geräten unterstützt, von Druckern bis hin zu Routern und Firewalls.

### Nutzung von Skripten
In Netzwerken, in denen kein NMS für die Überwachung verfügbar ist oder das vorhandene NMS bestimmte Funktionen nicht unterstützt oder sogar die Funktionalität des vorhandenen NMS-Tools erweitert, können Netzwerkadministratoren Skripte verwenden. Skripte verwenden häufige Befehle wie ping, netstat, lynx, snmpwalk usw., die von den meisten Netzwerkelementen unterstützt werden, um eine Aktion auszuführen, z. B. Informationen von Elementen zu sammeln, Änderungen an Gerätekonfigurationen vorzunehmen oder eine geplante Aufgabe auszuführen. Bash-Skripte, Perl, Python usw. sind gängige Skriptwerkzeuge, die von Netzwerkadministratoren verwendet werden.

## Netzwerküberwachung Design-Philosophie

Das Monitoring unterstützt Netzwerk- und Systemadministratoren dabei, mögliche Probleme zu identifizieren, bevor sie sich auf die Geschäftskontinuität auswirken, und die Ursache von Problemen zu finden, wenn etwas im Netzwerk schiefgeht. Egal, ob es sich um ein kleines Unternehmen mit weniger als 50 Knoten oder ein großes Unternehmen mit mehr als 1000 Knoten handelt, kontinuierliches Monitoring hilft dabei, ein leistungsstarkes Netzwerk mit geringer Ausfallzeit zu entwickeln und aufrechtzuerhalten.

Damit das Netzwerkmonitoring einen Mehrwert für ein Netzwerk darstellt, sollte das Monitoring-Design grundlegende Prinzipien berücksichtigen. Zum einen sollte ein Überwachungssystem umfassend sein und jeden Aspekt eines Unternehmens abdecken, wie zum Beispiel das Netzwerk und die Konnektivität, Systeme sowie Sicherheit. Es wäre auch wünschenswert, wenn das System einen ganzheitlichen Überblick über alles im Netzwerk bietet und Berichterstattung, Problemerkennung, -lösung und Netzwerkpflege einschließt. Darüber hinaus sollte jedes Überwachungssystem Berichte bereitstellen, die sich an unterschiedliche Zielgruppen richten - den Netzwerk- und Systemadministrator sowie das Management wie CEO, CIO und CTO. Am wichtigsten ist, dass ein Überwachungssystem weder zu komplex zu verstehen und zu bedienen sein sollte, noch grundlegende Berichterstattungs- und Drilldown-Funktionalitäten fehlen sollte.

### FCAPS

Das Netzwerkmanagement ist ein umfangreiches Feld, das verschiedene Funktionen umfasst. Die verschiedenen Ziele des Netzwerkmanagements sind in fünf verschiedene Kategorien unterteilt und gruppiert, nämlich Das Fehlermanagement (Fault management), Das Konfigurationsmanagement (Configuration management), Kontoüberwachung (Accounting), Leistungsmanagement (Performance management) und Sicherheitsmanagement (Security) - zusammen bekannt als **FCAPS**. In Netzwerken, in denen keine Abrechnung erforderlich ist, wird die Buchhaltung durch Administration ersetzt.

**Das Fehlermanagement (Fault management)** befasst sich mit dem Prozess der Erkennung, Isolierung und Behebung eines Fehlers, der im Netzwerk auftritt. Die Identifizierung potenzieller Netzwerkprobleme fällt ebenfalls unter das Fehlermanagement.

**Das Konfigurationsmanagement (Configuration management)** befasst sich mit dem Prozess der Erkennung, Isolierung und Behebung eines Fehlers, der im Netzwerk auftritt. Die Identifizierung potenzieller Netzwerkprobleme fällt ebenfalls unter das Fehlermanagement.

**Kontoüberwachung (Accounting)** gilt für Serviceprovider-Netzwerke, in denen die Nutzung von Netzwerkressourcen erfasst und die Informationen dann für die Abrechnung oder Kostenrückverfolgung verwendet werden. In Netzwerken, in denen keine Abrechnung erfolgt, wird die Buchhaltung durch Administration ersetzt, was sich auf die Verwaltung von Endbenutzern im Netzwerk mit Passwörtern, Berechtigungen usw. bezieht.

**Das Leistungsmanagement (Performance management)** umfasst das Management der Gesamtleistung des Netzwerks. Daten für Parameter, die mit der Leistung verbunden sind, wie Durchsatz, Paketverlust, Antwortzeiten, Auslastung usw., werden hauptsächlich mit SNMP gesammelt.

**Sicherheit (Security)** ist ein weiterer wichtiger Bereich des Netzwerkmanagements. Das Sicherheitsmanagement in FCAPS umfasst den Prozess der Kontrolle des Zugriffs auf Ressourcen im Netzwerk, zu denen Daten sowie Konfigurationen gehören, und den Schutz von Benutzerinformationen vor unbefugten Benutzern.

### Berichterstattung und Alarme

Die grundlegenden Komponenten des Netzwerk-Monitorings sind die Erfassung von Daten aus Netzwerkelementen sowie die Verarbeitung und Darstellung der gesammelten Daten in einem für den Benutzer verständlichen Format. Dieser Prozess selbst kann als Berichterstattung bezeichnet werden. Die Berichterstattung hilft dem Netzwerkadministrator dabei, die Leistung der Netzwerkknoten, den aktuellen Status des Netzwerks und das Normalverhalten im Netzwerk zu verstehen. Mit Daten aus Berichten kann ein Administrator fundierte Entscheidungen für die Kapazitätsplanung, Netzwerkpflege, Fehlerbehebung und Netzwerksicherheit treffen.

![Alt text](image.png)

Allein die Berichterstattung würde einem Administrator nicht helfen, ein leistungsstarkes Netzwerk aufrechtzuerhalten. Eine weitere wichtige Anforderung ist die Fähigkeit, potenzielle Probleme innerhalb des Netzwerks zu identifizieren. Während Berichte dabei helfen zu verstehen, was normal ist und den aktuellen Status des Netzwerks darstellen, helfen Alarme basierend auf Schwellenwerten und Auslösepunkten einem Netzwerkadministrator, mögliche Netzwerkprobleme in Bezug auf Leistung und Sicherheit zu identifizieren, bevor sie das Netzwerk zum Stillstand bringen. Alarme und Berichte ergänzen sich so, dass Alarme dem Administrator potenzielle Probleme mitteilen, und Berichte Daten bereitstellen, um die Ursache für Netzwerkprobleme zu identifizieren.

### Alarmierung

Jedes Netzwerk hat eine Basislinie, die beschreibt, was im Netzwerk hinsichtlich Netzwerkperformance und Netzwerkverhalten als normal angesehen wird. Die Basislinie für jedes Netzwerk unterscheidet sich voneinander. Wenn sich die Werte eines Parameters von einem etablierten Basislinienwert ändern, besteht das Potenzial, dass dies zu einem Problem führt, das die Netzwerkverfügbarkeit beeinträchtigen kann. In solchen Szenarien kann eine Alarmierung aufgrund der Abweichung vom Mittelwert bei der frühzeitigen Erkennung und Behebung von Problemen helfen, was wiederum zu einem reibungslosen Betrieb des Netzwerks mit weniger oder keinen Ausfallzeiten beiträgt. Die Alarmierung hilft Administratoren dabei, festzustellen, was im Netzwerk in Bezug auf Leistung und Sicherheit möglicherweise schief gehen könnte. Es gibt verschiedene Optionen, auf deren Basis ein Alarm generiert werden kann. Hier sind einige Begriffe, die mit Alarmen in Verbindung stehen:

#### Auslöser (Triggers)
Ein Auslöser bezieht sich auf das Ereignis, das dazu führt, dass ein Alarm generiert wird. Ein Ereignis kann hier die Änderung des Zustands eines Knotens oder eines Werts im Zusammenhang mit dem Knoten, die Abweichung vom Mittelwert eines Parameters, das Überschreiten des Schwellenwerts eines Parameters usw. sein.

#### Schwellenwerte, Wiederholungszählung und Zeitverzögerungen (Thresholds, repeat-count, and time delays)
Die meisten Alarme werden basierend auf Schwellenwerten generiert. Wenn der Basislinienwert im Zusammenhang mit einem Netzwerkparameter überschritten wird, tritt eine Schwellenwertüberschreitung auf und dies kann eingestellt werden, um einen Alarm auszulösen. Alarme können auch generiert werden, wenn Schwellenwerte basierend auf Wiederholungszählung und Zeit (z. B. 2-mal in 10 Minuten) verletzt werden.

#### Zurücksetzen (Reset)
Ein Alarm, der aufgrund einer Schwellenwertverletzung generiert wird, wird zurückgesetzt, wenn der Wert des Parameters, der den Alarm ausgelöst hat, zu seinem Basislinienwert zurückkehrt.

#### Unterdrückung und Duplikation (Suppression and de-duplication)
Bestimmte Schwellenwertverletzungen werden erwartet, auch wenn sie einen Schwellenwert überschreiten. In solchen Fällen werden Alarme unterdrückt. In anderen Fällen kann dasselbe Ereignis dazu führen, dass eine Schwellenwertverletzung bei mehreren Ereignissen auftritt, was wiederum mehrere Alarme auslöst. Um solche Alarmauslöser zu verhindern, unterstützen Überwachungssysteme die Duplizierung oder sogar die Konsolidierung von Alarmen basierend auf dem Ereignis, das sie ausgelöst hat.

## Gängigen Praktiken
Die gängigen Praktiken definieren die Grundlagen, die Teil des Netzwerk-Monitorings sind. Neben den hier spezifizierten gängigen Praktiken muss der Netzwerkadministrator das Design und die Anforderungen des von ihnen verwalteten Netzwerks verstehen und in der Lage sein, zusätzliche Überwachungsstrategien umzusetzen, um alle Metriken und Elemente im Netzwerk in ihren Aufgabenbereich zu integrieren.

### Verfügbarkeitsüberwachung

Die Verfügbarkeitsüberwachung definiert die Überwachung aller Ressourcen in der IT-Infrastruktur, um sicherzustellen, dass sie den Anforderungen der Organisation und ihrer Benutzer zur Verfügung stehen. Die heutige IT-Infrastruktur erfordert eine Verfügbarkeit von 100%, um den Geschäftsanforderungen gerecht zu werden. Das Netzwerk und die angebotenen Dienste im Netzwerk müssen jederzeit verfügbar sein, um die Geschäftskontinuität zu gewährleisten. Hier kann die Verfügbarkeitsüberwachung helfen.

Einige häufig verwendete Technologien für die Verfügbarkeitsüberwachung sind:

* Ping: Die am weitesten verbreitete Methode. ICMP-Pings werden an ein überwachtes Gerät gesendet, und basierend auf den Antworten wird die Verfügbarkeit eines Geräts oder Dienstes gemessen.

* Telnet: Wird verwendet, um die Verfügbarkeit eines Geräts in Netzwerken zu überprüfen, in denen Ping blockiert ist.

* SNMP: Wird verwendet, um die Verfügbarkeit oder den aktuellen Status eines Dienstes auf einem Gerät zu messen.

* WMI: Wird verwendet, um die Verfügbarkeit von Diensten auf Windows-Systemen zu überprüfen.

* IPSLA: Cisco-Funktion, die die Verfügbarkeit von WAN-Verbindungen und deren Kapazität zur Übertragung bestimmter Dienste messen kann.


### Interface Monitoring

In einem Netzwerk werden verschiedene Arten von Schnittstellen verwendet, wie zum Beispiel Fast Ethernet und Gigabit Ethernet bis hin zu den sehr schnellen Fiber-Channel-Schnittstellen. Die Schnittstelle an einem Gerät ist der Ein- und Ausgangspunkt für Pakete, die einen Dienst für die Organisation bereitstellen. Wenn es zu Fehlern, Paketverlusten oder sogar zum Ausfall der Schnittstelle selbst kommt, kann dies zu einer schlechten Benutzererfahrung führen.

Die Schnittstellenüberwachung beinhaltet das Überwachen der Schnittstellen an einem Gerät auf Fehler, Paketverluste, Verwerfungen, Nutzungsgrenzen usw. Die Informationen aus der Schnittstellenüberwachung helfen dabei, mögliche Netzwerkprobleme zu identifizieren, die die Ursache für eine schlechte Anwendungs- oder Dienstleistungsleistung sind.

Netzwerküberwachungssysteme verwenden Ping oder SNMP, um Schnittstellenstatistiken von Netzwerkgeräten zu sammeln. Während Ping unter Verwendung von ICMP-Paketen über Schnittstellenstatistiken wie Paketverlust, Round-Trip-Zeit usw. berichtet, hilft die datenbasierte Erfassung über SNMP dabei, die Bandbreitennutzung der Schnittstelle, die Verkehrsgeschwindigkeit auf der Schnittstelle, Fehler, Verwerfungen usw. zu überwachen. Zusammen helfen diese Informationen, Leistungsprobleme von Anwendungen im Netzwerk zu identifizieren.

### Festplattenüberwachung

Die Überwachung der Festplatte umfasst das ordnungsgemäße Management des Festplattenspeichers für eine effektive Speicherauslastung, die Überwachung der Festplattenleistung auf Fehler, Statistiken zu großen Dateien, freien Speicherplatz und Änderungen der Speicherauslastung, I/O-Leistung usw. Die Überwachung ermöglicht es Administratoren, frühzeitig Upgrades des Systems und des Speichers zu planen, Speicherprobleme zu erkennen und die Ausfallzeiten im Falle eines Problems zu reduzieren.

### Hardware

Ein Netzwerk umfasst viele Hardwaregeräte, wie zum Beispiel Geräte für Routing & Switching, Speicherung, Konnektivität, Anwendungsserver usw. Die Hardware bildet das Rückgrat der gesamten IT-Infrastruktur. Wenn eine für den täglichen Betrieb des Netzwerks wichtige Hardware ausfällt, kann dies auch zu Netzwerkausfallzeiten führen. Zum Beispiel kann ein fehlerhaftes Netzteil am Core-Switch oder Überhitzung des Edge-Routers zu einem Netzwerkausfall führen. Um einen reibungslosen Betrieb des Netzwerks zu gewährleisten, ist es wichtig, die Gesundheit und Leistung der Hardwaregeräte im Netzwerk zu überwachen.