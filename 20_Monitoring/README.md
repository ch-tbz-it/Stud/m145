# Monitoring - Überwachung & Auswertung


## Theorie
 - [Theorie](./Theorie/README.md)
 - [Wireshark Intro](./02_Wireshark_Theorie/README.md)

## Übungen
 - [Todo](./03_Wireshark_Uebung/README.md)

# Handlungskompetenzen
 - 2 Performance und Verfügbarkeit des Netzes mit Tools überwachen (Netzwerk Management System). Auswertungen/Logfiles interpretieren.
 - 2.1 Kennt mögliche Datenquellen (Netzwerkkomponenten und angeschlossene Endsysteme) für die Überwachung eines Netzwerkes.
 - 2.2 Kennt Tools zur Überwachung von Netzwerken.
 - 2.3 Kennt die relevanten Parameter zur Auswertung von Performance und Verfügbarkeit.
 - 2.4 Kennt die wichtigsten Darstellungsarten für die erhobenen Daten und und weiss wie diese interpretiert werden.