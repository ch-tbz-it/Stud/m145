# Übung: SNMP in Cisco Packet Tracer benutzen

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Sie können SNMP in Cisco Packet Tracer anweden und benutzen |

Lösen Sie die [Aufgabe](../media/m145_nwm_system_snmp_praxis_cisco_v1.1.pdf) im Cisco Packet Tracer.
* Dokumentieren Sie nach Angaben des Lehrers ihr Vorgehen
* Beanworten Sie die Fragen des PDFs