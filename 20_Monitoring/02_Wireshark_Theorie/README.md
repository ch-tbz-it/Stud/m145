# Der Network-Sniffer «Wireshark»

## Einführung

* Wireshark ist ein freies Programm zur Analyse und grafischen Aufbereitung von Datenprotokollen (Sniffer / Netzwerkmonitor).
* Findet man als Programm oder PortableAPP auf https://www.wireshark.org
* Bei Verwendung von Wireshark die rechtliche Situation beachten: Aus Gründen des Datenschutzes ist das Abhören oder Protokollieren von fremden Funkverbindungen verboten, sofern es vom Netzbetreiber nicht explizit erlaubt wurde. Darum: In der Lehrfirma unbedingt vorher um Erlaubnis fragen.
* Die erfassten Datenpakete werden in TCP/IP-Stack Schichten grafisch dargestellt:
* Netzzugang (MAC) / Network (IP) / Transport (Port) / Applikation (zB. HTTP)
* Wireshark zeigt bei einer Aufnahme sowohl die Protokoll-Header als auch den transportieren Inhalt an.
* Promiscuous Mode: Wireshark schneidet sämtlichen Datenverkehr mit. D.h. er zeichnet auch Datenpakete auf, die nicht an den eigenen PC gerichtet sind. Geräte innerhalb der Kollisionsdomäne können somit abgehört werden.
* Switches sorgen dafür, dass Netzwerkpakete dezidiert an den tatsächlichen Empfänger gesendet werden. Das erreichen sie, indem sie sich merken, an welchem Port welche MAC-Adresse zu finden ist. Demzufolge "sieht" auch eine Netzwerkkarte im Promiscuous Mode nur noch Broadcasts und an sie gerichtete Pakete. Somit ist es in solchen Netzen nicht ohne weiteres möglich, fremde Rechner auszuhorchen, ausser ihr Switch bietet Port Mirroring an. Kein Problem hat man allerdings beim Einsatz von Hub's, da diese immer nur eine Kollisionsdomäne bilden.
* In WLAN's den Netzwerkverkehr von fremden Personen abzuhören, geht nur in gänzlich unverschlüsselten WLAN's, wie z.B. öffentlichen Hotspots. Sobald WEP oder WPA implementiert ist, sieht man zwar die Verbindung (MAC, IP, Port), was aber in der Applikationsschicht vor sich geht, bleibt verborgen.
(Hinweis: WEP sollte nicht mehr verwendet werden, weil gehackt und unsicher.)
* Wireshark bietet Displayfilter und Capturefilter. Filter: Syntax für Filterregeln (Verknüpfungen).
* Bei WLAN kann allenfalls der WLAN-Adapter für das Sniffing ungeeignet sein. Im Normalzustand wird jeder WLAN-Adapter nur die Datenpakete durchleiten, die an ihn adressiert sind. Hier sollte der WLAN-Adapter aber den Monitor Mode und Injections beherrschen.
**Nutzen sie Wireshark als Diagnosetool und nicht als Spyware!**

Falls Wireshark nicht funktionieren sollte oder blockiert ist, probieren sie folgendes aus: Suchen sie im Wireshark-Ordner (zu finden im Ordner «Programme») das Programmfile «wireshark.exe» und führen sie es als Administrator aus (Rechtsklick «Als Administrator ausführen»)

## Was bietet mir Wireshark?

* Um die Frage zu klären, wer auf meinem PC nach Hause "telefoniert".
* Zur Fehlersuche oder bei der Bewertung des Kommunikationsinhalts.
* Welcher Dienst geht über welche IP-Adresse und Portnummer
* Der häufigste Grund, einen Netzwerkmonitor einzusetzen, sind Firewalls und andere Sicherheitsprogramme, die auch legitime Kommunikation unterbinden, wenn sie nicht richtig konfiguriert sind. Derartige Probleme lassen sich mit einem Sniffer in vielen Fällen erkennen und lösen.
* Zu Schulungszwecken um das TCP/IP-Protokoll besser zu verstehen.

**Verschlüsselte Informationen/Pakete bleiben geheim (HTTPS, TLS, SSL)**

## Wiresharkfilter anwenden

Um die Datenflut etwas einzudämmen, sollte man Filter einsetzen. Einerseits gibt es Capture-Filter und andererseits Display-Filter. Capture-Filter (z.B. tcp port 80) sind nicht mit Display-Filtern (z.B. tcp.port == 80) zu verwechseln. Capture-Filter sind viel eingeschränkter und werden verwendet, um die Grösse einer Rohpaketerfassung zu reduzieren. Display-Filter werden verwendet, um einige Pakete aus der Paketliste zu verstecken.
Capture-Filter werden vor Beginn einer Paketerfassung festgelegt und können während der Erfassung nicht geändert werden. Display-Filter hingegen haben diese Einschränkung nicht und können jederzeit geändert werden.
Im Hauptfenster findet man den Capture-Filter direkt über der Schnittstellenliste und im Schnittstellendialog. Der Display-Filter kann über der Paketliste geändert werden.
Im Internet findet sich dazu viel entsprechendes Material. Zum Beispiel hier: https://wiki.wireshark.org/Home

### Ein paar nützliche Display-Filter:

```
(Capture-Filer haben eine andere Syntax als Display-Filter)

Capture-Fenster löschen, ohne Wireshark neu zu starten und Filter neu setzen: (3 Versionen!)
→ Auf die grüne Haifischflosse neben der roten "Stop" Taste klicken
→ Menü Aufzeichnen > Neustart
→ Ctrl-R

Zeigt nur den Datenverkehr im 192.168.1.0-Netzwerk:
ip.src==192.168.1.0/24 and ip.dst==192.168.1.0/24

Zeigt nur den Datenverkehr von und zu dieser IP-Adresse:
ip.addr == 192.168.1.15 ist identisch mit
ip.src == 192.168.1.15 or ip.dst == 192.168.1.15

Zeige allen Datenverkehr ausser den von und zu dieser IP-Adresse
! ( ip.addr == 192.168.1.15 )

Zeigt nur SMTP (Port25) und ICMP-Datenverkehr
tcp.port eq 25 or icmp
Zeigt nur SMTP (Port25) und ICMP-Datenverkehr

Zeigt nur DHCP-Datenverkehr:
dhcp.option.type == 53

DNS Anftage für edu.juergarnold.ch
dns.qry.name == "edu.juergarnold.ch"

dns and ip.dst==159.25.78.7 or dns and ip.src==159.57.78.7

Zeigt «Post» von einem (unverschlüsselten!) Webseitenformular:
http.request.method == "POST"
```

## Wireshark und verschlüsselte Webseiten

Beim Aufruf von verschlüsselten Webseiten wirkt oberhalb TCP das Verschlüsselungsprotokoll SSL/TLS (Heutzutags TLS, SSL war sein Vorgänger). Das bedeutet, dass Wireshark oberhalb TLS nichts anzeigen kann, da verschlüsselt. Dies ist auch erwünscht, weil dabei für Drittpersonen nicht einsehbar ist, was Webclient und Webserver miteinander austauschen, insbesondere Formulare, Passwörter etc. Ab HTTP 1.1 wird bei einem Webseitenaufruf die URL mitgegeben, weil es ja durchaus üblich ist, dass unter einer IP-Adresse mehrere Webseiten gehostet werden und darum der Server wissen muss, welche Webseite nun gemeint ist. Nun könnte man meinen, mit TLS wäre es nicht mehr möglich herauszufinden, welche Webseite aufgerufen wurde, weil sich diese Angabe nur im Application-Layer befindet. Dem ist aber nicht so. Im TLS-Layer findet man nämlich auch einen entsprechenden Hinweis. Und das geht so:

* Wireshark-Displayfilter: ssl.handshake.extensions_server_name (Zeigt alle "Client Hello")
* Eine der angezeigten Zeilen auswählen und TLS (Transport Layer Security) aufklappen.
* Danach: TLSv1.x Record Layer: Handshake Protocol: Client Hello
* Danach: Handshake Protocol: Client Hello
* Danach: Extension: server_name
* Danach: Server Name Indication extension
* Und man erhält: Server Name: xxxxxx.yyyyy.zzz
* Nun könnte man Rechtsklick → "Als Spalte anwenden" und hätte im oberen Fenster alle URLs angezeigt.

Hintergrund-Info: Server Name Indication (SNI) ist eine Erweiterung von TLS, um mehrere verschlüsselte Webseiten auf einem Server aufzurufen. Jede Domain hat ein anderes Zertifikat und der Server muss wissen, welches er liefern muss. Das wird ihm unverschlüsselt über die SNI vom Client geliefert.
