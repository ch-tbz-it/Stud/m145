# 145 - Netzwerk betreiben und erweitern

# Kompetenz
Performance und Verfügbarkeit eines Netzwerks überwachen und Ergebnisse interpretieren. Netzwerke nach Vorgabe mit WLAN /VLAN erweitern und entfernte lokale Netze sicher verbinden.

# Modulbeschreibung
Siehe [modulbaukasten.ch](https://www.modulbaukasten.ch/module/145)

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.