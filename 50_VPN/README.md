# VPN


# Handlungskompetenzen
 - 6 Entfernte lokale Netze sicher verbinden.
 - 6.1 Kennt Möglichkeiten und typische Einsatzgebiete für eine sichere Verbindung entfernter LANs.
 - 6.2 Kennt Vorteile und Nachteile von VPN-Technologien für eine sichere Verbindung.
 - 6.3 Kennt den prinzipiellen Unterschied zwischen WAN-Access, WAN-Core-Netzwerk und VPN Verbindungen.

# Ressourcen (Theorie)
* Was ist der Unterschied zwischen Site-to-Site VPN und VPN Clients mit VPN-Serververbindung? (computerweekly.com)
https://www.computerweekly.com/de/antwort/Was-ist-der-Unterschied-zwischen-Site-to-Site-VPN-und-VPN-Clients-mit-VPN-Serververbindung

* Was ist VPN und wie funktioniert es? | Kaspersky
https://www.kaspersky.de/resource-center/definitions/what-is-a-vpn

* Wie funktioniert VPN? Einfach erklärt - CHIP
https://praxistipps.chip.de/wie-funktioniert-vpn-einfach-erklaert_40812

* Virtual Private Network – Wikipedia
https://de.wikipedia.org/wiki/Virtual_Private_Network

# Ressourcen (Praxis)

* Ubuntu 20.04 LTS Set Up OpenVPN Server In 5 Minutes:
 https://www.cyberciti.biz/faq/ubuntu-20-04-lts-set-up-openvpn-server-in-5-minutes/

* How To Guide: Set Up & Configure OpenVPN Client/server VPN | OpenVPN
https://openvpn.net/community-resources/how-to/

* How to Install and Configure OpenVPN Server on Windows
 https://woshub.com/install-configure-openvpn-server-windows/

* WireGuard: fast, modern, secure VPN tunnel
https://www.wireguard.com/

# Tools

* https://openvpn.net/
* https://tunnelblick.net/
* https://tunnelblick.net/
* WireGuard: fast, modern, secure VPN tunnel (https://www.wireguard.com/)
* Liste mit öffentlich zugänglichen VPN-Server:
https://ipspeed.info/freevpn_l2tpipsec.php?language=en

