# VLAN

Wenn Sie ein Netzwerk aufbauen wollen, kaufen Sie sich einen Switch und bauen so das Netzwerk auf.

Wollen Sie ein weiteres Netzwerk, kaufen Sie einen weiteren Switch und bauen das nächste Netzwerk auf. Jetzt haben Sie zwei voneinander unabhängige Netzwerke.
Das gleiche Prinzip benutzen VLANs – aber eben virtuell…

Ein grosses Netzwerk wird in unabhängige kleinere Netzwerke zerlegt. Um dabei Hardware-Kosten zu sparen, wird das aber nicht physikalisch wie im obigen Beispiel gemacht, sondern virtuell innerhalb vom Switch (oder den Switches).

Virtuelle Netzwerke laufen auf der gleichen Hardware, sind aber untereinander abgeschottet. Dadurch können Sie in einem grossen System viele kleinere Systeme aufbauen. So lassen sich zum Beispiel Hierarchien und Zugriffe viel besser kontrollieren. Aber auch die Sicherheit wird deutlich verbessert. Sensible Daten werden so vor neugierigen Blicken geschützt.

## Handlungskompetenzen:
 - 4 Netzwerke in virtuelle LAN aufteilen und konfigurieren.
 - 4.1 Kennt die Möglichkeiten zur physikalischen und logischen Gliederung eines Netzwerkes und ihre Auswirkungen auf Performance und Verfügbarkeit.
 - 4.2 Kennt die VLAN Typen (tagbasiert, portbasiert und dynamisch) und typische Konfigurationsmöglichkeiten der Geräte.

## Theorie

 - [VLAN Theorie](Theorie/README.md)

## Übungen

 - [BeASwitch mit VLAN](01_BeASwitch_VLAN/README.md)
 - [Cisco Packet Tracer](02_PacketTracer/README.md)

