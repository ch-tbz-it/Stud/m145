# VLAN mit Cisco Packet Tracer

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung mit Cisco Packet Tracer |
| Zeitbudget  |  8 Lektion |
| Ziele |  |

---

Alle aufgeführten Cisco Packet Tracer Labore findest du auf dem Sharepoint zum Modul. 

Link zum entsprechenden Ordner: [students/it/_read-only/M145/04 - Cisco Packet Tracer/30_VLAN](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/it/_read-only/M145/04%20-%20Cisco%20Packet%20Tracer/30_VLAN?csf=1&web=1&e=wDaq7S).

**Hinweis:** Falls der Link zum Sharepoint nicht funktioniert, stelle sicher, dass du mit dem TBZ Microsoft Login eingeloggt bist. Falls der Link immer noch nicht funktioniert, frage bei der Lehrperson nach den entsprechenden Dateien. 

Folgende Übungen in der aufgeführten Reihenfolge mit dem Cisco Packet Tracer werden empfohlen:
 - `07_VLAN Configuration.pka` - VLAN Access Port Konfiguration
 - `08_Trunk Configuration.pka` - VLAN Trunk Port Konfiguration
 - `09_Implement VLANs and Trunking.pka` - Komplette VLAN Konfiguration nach Vorgabe
 - `10_Investigate a VLAN Implementation.pka` - Verhalten von Broadcast in einem Netzwerk mit VLAN beobachten
 - `11_Inter-VLAN Routing Challenge.pka` - **Challenge Übung**: Wie in der Praxis anzutreffen: Kombination aus VLAN Konfiguration und Routing

## Nützliche Befehle

### VLAN mit Name und Nummer in der VLAN Datenbank hinterlegen

In nachfolgenden Beispiel wird das VLAN mit der VID 20 mit dem Namen `students` hinterlegt:
```
S1# configure terminal
S1#(config)# vlan 20
S1#(config-vlan)# name Students
S1#(config-vlan)# exit
```

## Ein Port als Access Port konfigurieren

 - Access Port: Üblicherweise ein Port auf dem nur ein VLAN untagged zugewiesen ist und ein Endgerät angeschlossen ist
 - Trunk Port: Mehrere VLANs mit Tagged. Üblicherweise Verbindung zwischen den Switchen

Im nachfolgenden Beispiel wird auf dem Interface `FastEthernet 0/11` der Port als *Access Port* im VLAN 10 konfiguriert:
```
S1# configure terminal
S1#(config)# interface f0/11
S1#(config-if)# switchport mode access
S1#(config-if)# switchport mode vlan 10
```

## Ein Port als Trunk Port Konfigurieren

In nachfolgenden Beispiel werden die zwei GigabitEthernet Ports 0/1 und 0/2 als Trunk Ports konfiguriert. Zusätzlich wird das VLAN 99 als natives VLAN gesetzt (Standardmässig ist VLAN 1 *native*):
```
S1# configure terminal
S1 (config)# interface range g0/1 – 2
S1 (config)# switchport mode trunk
S1 (config)# switchport trunk native vlan 99
```

