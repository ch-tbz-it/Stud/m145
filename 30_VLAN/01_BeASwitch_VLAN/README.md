# BeASwitch - mit VLAN

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenart  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie verstehen die grundlegende Funktionsweise von Switching mit VLAN |

BeASwitch aus den Repetitionsübung hat die Option dieselbe Übung mit VLAN zu lösen. 

Löse [BeASwitch](../../05_Repetition/02_BeASwitch/README.md) nochmals, setze diesmal einen Hacken bei `use VLAN`.

![BeASwitch](media/beaswitchconfig.JPG)