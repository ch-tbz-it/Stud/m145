# Theorie - VLAN

# Was ist ein VLAN?

Wie lief es bisher…

Wenn Sie ein Netzwerk aufbauen wollen, kaufen Sie sich einen Switch und bauen so das Netzwerk auf.

Wollen Sie ein weiteres Netzwerk, kaufen Sie einen weiteren Switch und bauen das nächste Netzwerk auf. Jetzt haben Sie zwei voneinander unabhängige Netzwerke.

Das gleiche Prinzip benutzen VLANs – aber eben virtuell…

Ein grosses Netzwerk wird in unabhängige kleinere Netzwerke zerlegt. Um dabei Hardware-Kosten zu sparen, wird das aber nicht physikalisch wie im obigen Beispiel gemacht, sondern virtuell innerhalb vom Switch (oder den Switches).

Virtuelle Netzwerke laufen auf der gleichen Hardware, sind aber untereinander abgeschottet. Dadurch können Sie in einem grossen System viele kleinere Systeme aufbauen. So lassen sich zum Beispiel Hierarchien und Zugriffe viel besser kontrollieren. Aber auch die Sicherheit wird deutlich verbessert. Sensible Daten werden so vor neugierigen Blicken geschützt.

#  Warum VLAN?

-   Strikte Trennung: VLANs trennen (segmentieren) Netzwerke auf Layer 2
    komplett. Systeme zwischen verschiedenen VLANs «sehen» und stören sich nicht und können keine (unerwünschten) Daten austauschen, wie z.B. unerwünschte Broadcasts.
-   Flexibilität bei der Zuordnung von Endgeräten zu Netzwerksegmenten,
    unabhängig vom physischen Standort des Geräts, also logische Gruppen
    innerhalb der physikalischen Topologie.
-   Priorisierung von bestimmtem Datenverkehr wie z.B. VoIP.
-   Mehr Dienste im Netz (Drucker, IP-Telefonie, IP-TV, etc.) ziehen eine
    grössere Komplexität nach sich. Diese Komplexität kann man reduzieren, indem man konsequent für jeden Dienst ein eigenes VLAN im Netz bezeichnet.
-   Die Sicherheit ist bei VLANs besser als bei rein geswitchten Netzen, die durch Angriffsmöglichkeiten wie MAC-Spoofing und MAC-Flooding unsicher sind.
-   Sicherheitsrelevante Systeme können in ein separates VLAN gelegt und durch entsprechende Massnahmen besser geschützt werden.
-   Zur Verbindung von VLANs kommen Router (Layer 3 Switches) zum Einsatz, die gegen Layer-2-Attacken unempfindlich sind. Zusätzlich bietet Routing die Möglichkeit, Firewalls auf Layer-3-Basis einzusetzen.

# Wie funktioniert ein VLAN?

Die Zuordnung der Teilnetzwerke auf dem Switch (oder den Switches) kann auf zwei Arten erfolgen, als Portbasierte VLANs (Untagged) der als Tagged VLANs.

## Portbasierte VLANs

Mit portbasierten VLANs unterteilen Sie **einen einzelnen physischen Switch** einfach **auf mehrere logische** Switches (VLANs). Dabei wird jedem Port ein VLAN zugewiesen.

![https://www.thomas-krenn.com/de/wikiDE/images/7/72/VLAN-Grundlagen-Beispiel-1.png](media/switch_green_red_ports.png)

Im folgenden Beispiel teilen wir einen physischen 8-Port Switch (Switch A) auf zwei logische Switches auf:

| **Switch A**    |             |                           |
|-----------------|-------------|---------------------------|
| **Switch-Port** | **VLAN ID** | **angeschlossenes Gerät** |
| 1               | 1 (grün)    | PC A-1                    |
| 2               |             | PC A-2                    |
| 3               |             | -                         |
| 4               |             | -                         |
| 5               | 2 (orange)  | PC A-5                    |
| 6               |             | PC A-6                    |
| 7               |             | -                         |
| 8               |             | -                         |

Obwohl alle PCs an einem physischen Switch angeschlossen sind, können aufgrund der VLAN Konfiguration nur folgende PCs jeweils miteinander kommunizieren:

-   PC A-1 mit PC A-2
-   PC A-5 mit PC A-6

Nehmen wir an, dass im Nachbarraum ebenfalls vier PCs stehen. Nun sollen PC B-1 und PC B-2 mit PC A-1 und PC A-2 im ersten Raum kommunizieren können. Ebenfalls soll die Kommunikation zwischen PC B-5 und PC B-6 aus Raum 2 mit PC A-5 und PC A-6 im Raum 1 möglich sein.

Im Raum 2 haben wir wieder einen Switch:

| **Switch B**    |             |                           |
|-----------------|-------------|---------------------------|
| **Switch-Port** | **VLAN ID** | **angeschlossenes Gerät** |
| 1               | 1 (grün)    | PC B-1                    |
| 2               |             | PC B-2                    |
| 3               |             | -                         |
| 4               |             | -                         |
| 5               | 2 (orange)  | PC B-5                    |
| 6               |             | PC B-6                    |
| 7               |             | -                         |
| 8               |             | -                         |

Damit die beiden VLANs hier verbunden werden können, benötigen wir bei portbasierten VLANs **zwei** Kabel, je eines für die entsprechenden VLANs:

-   von Switch A Port 4 zu Switch B Port 4 (für das VLAN 1)
-   von Switch A Port 8 zu Switch B Port 8 (für das VLAN 2)

![https://www.thomas-krenn.com/de/wikiDE/images/d/d0/VLAN-Grundlagen-Beispiel-2.png](media/two_switches.png)

*Portbasiertes VLAN mit zwei Switches.*

## Tagged VLANs

Bei **Tagged VLANs** können mehrere VLANs über einen **einzelnen Switch-Port** genutzt werden. Die einzelnen Ethernet Frames bekommen dabei ein **Tag** hinzugefügt, in dem die VLAN-ID vermerkt ist, zu dessen VLAN das Frame gehört.  

Wenn im gezeigten Beispiel beide (!) Switches tagged VLANs beherrschen, kann damit die gegenseitige Verbindung mit **einem einzelnen Kabel** erfolgen. Diese eine Verbindung mit den tagged VLANs wird **Trunk** genannt:

![https://www.thomas-krenn.com/de/wikiDE/images/4/4f/VLAN-Grundlagen-Beispiel-3.png](media/twoswitchestagged.png)

In der Norm IEEE 802.1q ist definiert, wie die Tags in das Ethernet-Frame
eingefügt werden.

Zusatzinfo: Die Ports werden zwischen **Access-Ports** und **Trunk-Ports** unterschieden.  
Das **Access-Port** dient dazu, **ein** Gerät mit dem Switch zu verbinden, über diese Port wird **nur ein** zugewiesenes VLAN geführt.  
Ein Trunk-Port, ist in der Lage, mehrere VLANS zu transportieren, z.B. zwischen Switches oder zu einem Gerät, das mehrere VLANs bedient wie z.B. Server oder WLAN-APs.

##  Aufbau Ethernet Frame

Der VLAN Tag kommt in einem Ethernet Frame nach den MAC Adressen. Es enthält u.a. die VLAN-ID, die das Frame dem VLAN zuordnen:

![Ethernet-Frame-VLAN-Tag.png](media/ethernetframe.png)

Bei tagged VLANs nach IEEE 802.1q werden die Pakete **vom Endgerät** (z. B. Tagging-fähigem Server oder Access-Point) **versehen**. Wenn das einspeisende Gerät selber kein tagging kennt, kann das der Switch für das Gerät übernehmen. 
Zu diesem Zweck wird dem Port eine **PVID** (Port VLAN ID) zugewiesen. Jedes **untagged Frame** das an diesem Port eingeht, wird mit dem entsprechenden VLAN-Tag (PVID) versehen und wird damit zu einem **tagged Frame.**

Achtung: Ein Frame mit Tags wird i.d.R. von einem Gerät, dass Tags nicht kennt als ungültig betrachtet und verworfen.

##  VLAN und Routing

VLANs arbeiten auf Layer 2 und sind damit von der IP-Adressierung grundsätzlich nicht betroffen, aber…

![Bildergebnis für layer 3 switchsymbol](media/layer3switch.jpeg)

VLANs führen zu einer kompletten Trennung auf Layer 2. Das heisst aber auch, dass zwischen den VLANs kein Datenverkehr möglich ist. Wenn doch zwischen den VLANs Datenverkehr nötig ist, muss auf **Layer 3 geroutet** werden. Damit das aber geht ist es zwingend nötig, dass **jedes VLAN** **ein eigenes (Sub) IP-Netz bildet**. Diese IP-Netze werden dann über Router verbunden. Damit die Router die getagten Frames unterscheiden können (z.B. auf einem Trunk), müssen diese Router **tagging verstehen**. Diese Geräte werden allgemein als **Layer 3 Switches** oder **Routing Switches** bezeichnet.

##  Verständnisfragen

Beantworten Sie die nachfolgenden Fragen im Forms (Einzelaufgabe):

Den Link erhalten Sie von der Lehrperson.

1.  Auf welchem ISO-OSI-Layer arbeitet ein normaler Switch?
2.  Kann ich mehrere Subnetze über einen normalen, nicht VLAN-fähigen Switch
    führen?
3.  Nennen Sie Gründe, ein physikalisches LAN in virtuelle Netze aufzuteilen
4.  Darf ein VLAN Netzwerk als «Sicher» eingestuft werden?
5.  Was versteht man unter «Portbasiertem VLAN»?
6.  Was versteht man unter «Tagged VLAN»?
